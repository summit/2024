# Gerrit User Summit 2024 - Schedule

## Hackathon, 7-9th October (Mon-Wed)

| Time PDT | Agenda                              |
| -------- | ----------------------------------- |
| 8:00     | [Check-in](location.md#checkin)     |
| 8:00     | Coffee/drinks in the Hackathon room |
| 9:00     | Discuss Hackathon topics            |
| 9:30     | Coding and discussions              |
| 12:15    | Lunch in the Hackathon room         |
| 14:00    | Coding and discussions              |
| 19:00    | End of the day                      |

## User Summit, 10th & 11th October (Thu & Fri)

Introduction, intermediate and advanced sessions on Gerrit Code Review.

The **Call For Papers is officially open**, [submit your talk proposal](cfp.md)
directly through [Gerrit Code Review](https://gerrit-review.googlesource.com/#/admin/projects/summit/2024).

The voting and final **selection of the agenda is totally driven by Community**,
the review score defines the chart of best rated talks.

Friday afternoon will be [unconference](https://tinyurl.com/ybe482yh) style, participants will be
able to suggest topics they'd like to either hear or speak about.

> **Follow the instructions** described in the [sessions proposal section](cfp.md)
> and be an active part of the Summit by sharing your experience and providing
> your feedback on how to improve Gerrit Code Review.

### Thursday 10th October

| Time PDT | Session                                                                                                          |
| -------- | ---------------------------------------------------------------------------------------------------------------- |
| 08:30    | [Check-in](location.md#checkin) / Breakfast in the summit room                                                   |
| 09:30    | [Running k8s-gerrit in production](sessions/k8s-gerrit-sap.md)                                                   |
| 10:15    | [Qualcomm's Gerrit management overview from Devops/SRE](sessions/devops-qcom.md)                                 |
| 11:00    | Coffee & Networking|
| 11:10    | [Migration from Gerrit 2.13 to 3.9 on a per-project basis: Challenges and Lessons Learned](sessions/migrating-from-2.13-to-3.9.md)|
| 11:55    | [Hackathon Outcome: generalising chatgpt plugin](lightning-talks/hackathon-ai-code-review-plugin.md) |
| 12:10    | [Adding multi-site support to K8s-Gerrit](sessions/k8s-gerrit-multi-site.md)|
| 12:35    | Lunch & Networking                                                                                               |
| 14:15    | [Repeated Builds During Code Review: The Case of OpenStack](sessions/repeated-builds-during-code-review.md)      |
| 15:00    | [Hackathon Outcome: Porting MIDX files to JGit](lightning-talks/hackathon-midx-outcome.md)                       |
| 15:15    | [Hackathon Outcome: Messageoftheday plugin and JGit DiffFormatter improvements](lightning-talks/hackathon-messageoftheday-and-jgit-diff-formatter-improvements.md)|
| 15:30    | Coffee & Networking|
| 15:40    | [Tracking Git Repository Health with Metrics: Insights and Challenges](lightning-talks/what-s-up-in-your-repo.md)|
| 15:55    | [Creating Quotas using Task Parker](lightning-talks/quotas-using-task-parker.md)                                  |
| 16:10    | Unconference prep / start                                                                                                 |
| 17:00    | End of the day                                                                                                   |

### Friday 11th October

| Time PDT | Session                                                                                                           |
| -------- | ----------------------------------------------------------------------------------------------------------------- |
| 08:30    | [Check-in](location.md#checkin) / Breakfast in the summit room                                                    |
| 09:30    | [What's coming in 3.11](sessions/whats-new-gerrit-3.11.md)                                                        |
| 10:00    | [Q&A with the Gerrit maintainers](sessions/maintainers-qa.md) |
| 10:30    | [Challenges after Qualcomm's upgrade to 3.5 from 2.7](sessions/challenges-after-upgrading-to-3.5-from-2.7.md)     |
| 11:00    | [Upgrade to Gerrit 3.5 takeaways](sessions/upgrade-to-3.5.md)                                                     |
| 11:15    | [Sustaining Git repo performance under heavy load](sessions/sustaining-git-repo-performance-under-heavy-load.md)  |
| 12:00    | Lunch & Networking                                                                                                |
| 13:45    | [Unconference](https://tinyurl.com/ybe482yh)
| 17:00    | End of the day                                                                                                    |
