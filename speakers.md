# Gerrit User Summit 2024 - Speakers

### Daniele Sassoli - GerritForge {#dsassoli}

Daniele is a Senior Software Engineer with a long experience in highly scalable
systems hosted in the cloud. As a member of the GerritForge team he has had the
chance to design, mantain and improve high throughput multi-site installations.
He also gets to learn from the team's wealth of experience in all things Gerrit
and contribute to the long list of plugin's maintained by them.

[LinkedIn](https://www.linkedin.com/in/danielesassoli/)

### Nasser Grainawi - Qualcomm Innovation Center, Inc {#nasser}

Nasser is a Senior Staff Software Engineer who has been working with Gerrit
since the beginning. Initially posting to the repo-discuss list, he made his
first contribution in 2011, became a Maintainer in 2020, and has been a
Community Manager since 2021. He enjoys understanding and solving performance
and other "big system" challenges, both in Gerrit and in other systems
integrating with Gerrit.

[LinkedIn](https://www.linkedin.com/in/nassergrainawi/)

### Kaushik Lingarkar - Qualcomm Technologies, Inc {#kaushikl}

Kaushik joined Qualcomm over 15 years ago and has had a variety of
roles, often related to Gerrit or workflows using Gerrit. He made his
first Gerrit contribution in 2013 and has made many more since 2020 as a
full-time member of the Qualcomm Gerrit team. He became a Gerrit Maintainer in
2022.

[LinkedIn](https://www.linkedin.com/in/kaushiklingarkar/)

### Vasdev Gullapalli - Qualcomm Technologies, Inc {#vasdevg}

Vasdev joined Qualcomm in 2013, and he is a senior staff Devops/SRE engineer.
He has worked in various teams and currently managing various git based
systems as a Technical Lead / Platform Owner.

[LinkedIn](https://www.linkedin.com/in/vasdev-gullapalli-b3450374/)

### Tushar Himmat Dahibhate - Qualcomm Technologies, Inc {#tdahibha}

Tushar joined Qualcomm over four years ago and is a Senior DevOps/SRE Engineer.
He has been involved in various development activities, such as migrating
on-premise systems to Kubernetes, designing and developing automated
deployment pipelines, and enabling monitoring and alerting via Datadog etc

[LinkedIn](https://www.linkedin.com/in/tushar-dahibhate/)

### Fabio Ponciroli - GerritForge {#fponciroli}

Fabio is a Senior Software Engineer at GerritForge, where he contributes to the OpenSource Gerrit Code Review project.
He has been involved since the beginning in the design and development of Gerrit DevOps analytics tools,
multi-site plugin and many other.
He has also created the Gatling protocol manager for Git which has finally provided a real E2E test suite
for the validation of the Gerrit Code Review releases.
He has extensive experience in working on backend systems, on-premise and cloud-based,
with different programming languages, such as Scala, Java, NodeJS and related ecosystems.

[LinkedIn](www.linkedin.com/in/fponciroli)

### Shane McIntosh - University of Waterloo {#smcintosh}

Shane McIntosh is the current Ross & Muriel Cheriton Faculty Fellow and an
Associate Professor in the Cheriton School of Computer Science at the University
of Waterloo, Canada. At U. Waterloo, Shane directs the Software Repository
Excavation and Build Engineering Labs (a.k.a., the Software REBELs). Shane and
his trainees investigate how peer code review can be made more effective, how
devops pipelines can be made more efficient, and how software can be produced
with higher quality.

[LinkedIn](www.linkedin.com/in/shane-mcintosh/)

### Matthias Sohn - SAP {#msohn}

Matthias Sohn is a Product Expert at SAP hacking on versioning systems since 25 years
and leading its Gerrit team. He is a Gerrit maintainer and community manager and leads
the JGit and EGit projects at the Eclipse Foundation.

[LinkedIn](https://www.linkedin.com/in/matthiassohn)

### Trevor Getty - Cirata {#trevorgetty}

Trevor Getty is a Software Architect and Technical lead of the DevOps team at Cirata.
He has been involved with the ongoing development and support of several replicated SCM products including
Subversion, Git and Gerrit Multisite products for medium to large scale sites.
He has been contributing to Gerrit since 2018, and uses the Gerrit Summit and Hackathons as a great 
opportunity to integrate fixes and features from the Cirata replicated forks of JGit and Gerrit into the main 
community versions.  
A previous hackathon allowed the merging of Offline Reindex performance changes, including lucene configuration extensions.
This was to allow considerably better throughput of indexing operations in versions of gerrit from 2.13 onwards, 
where the indexing performance of some operations was reduced from weeks to hours.

[LinkedIn](https://uk.linkedin.com/in/trevor-d-getty-7050b722)

### Luca Milanesio - GerritForge {#luca}

Luca is co-founder of GerritForge and has over 32 years of software development
and application lifecycle management experience.
He has been Gerrit contributor since 2012, Gerrit release manager,
member of the Gerrit Engineering Steering Committee and maintainer of
GerritHub.io, the Open Service for Gerrit Code Review on top of GitHub
repositories.

[LinkedIn](https://www.linkedin.com/in/lucamilanesio/)

### Eryk Szymanski - Digital.ai {#eszymanski}

Eryk is leading Digital.ai's TeamForge Git Engineering team in Berlin.

[LinkedIn](https://www.linkedin.com/in/eryk-szymanski-94b92ba/)
