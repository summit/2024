# Conduct Policy

The Gerrit Community reserves the right to refuse admittance to, or remove any
person from the summit premises at any time in its sole discretion. The Gerrit
Community [Code of Conduct](https://www.gerritcodereview.com/codeofconduct.html)
will apply.
