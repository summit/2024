# Hackathon Outcome: Porting MIDX files to JGit

During the Hackathon I'll be trying to port MultiPack Bitmap Index (MIDX)
to the JGit codebase. Bitmap indexes are a crucial optimization for a performant
repository, unfortunately they currently work only for a single pack-file.
This means that, currently, other optimizations like `git exp-roll` (exponential
roll), aren't as effective as they could be as using them will invalidate the
current bitmap capabilities.

Introducing MIDX bitmaps to the JGit codebase will enable much greater
flexibility for SCM managers all around the world.

In this talk I'll expose the progress we made during the Hackathon and outline
the next steps to make MIDX files a reality in JGit.

*[Daniele Sassoli, Senior Engineering Manager / GerritForge Inc.](../speakers.md#dsassoli)*
