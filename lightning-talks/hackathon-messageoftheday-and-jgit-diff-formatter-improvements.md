# messageoftheday plugin and jgit DiffFormatter improvements

messageoftheday plugin can be handy if it is more versatile
and is usable for multi-primary environments. A few updates
were done to the plugin to:

- Allow configuring the dirs containing the message content and config
- Add a REST API to set the banner message
- Allow users with 'updateBanner' capability to update the message
- Provide a way to update the message and set its age via the UI

JGit's DiffFormatter can use some improvements to have the same format
as the C implementation of Git.

*[Kaushik Lingarkar, Engineer / Qualcomm Technologies, Inc](../speakers.md#kaushikl)*
