# Tracking Git Repository Health with Metrics: Insights and Challenges

In today's fast-paced development environment, maintaining the health and performance of a Git repository is crucial yet often overlooked.
Repositories can grow large and complex, and without proper oversight, they can severely impact the efficiency of development teams.
In this talk, we'll dive into two essential tools—the [Git Repo Metrics Plugin](1) and the [GHS Metrics Collector Plugin](2) — which help in
tracking the dimensions, shape, and performance of your repository.

We'll explore the inherent challenges in monitoring repository growth and understanding how it affects performance.
From slowing down CI pipelines to increasing clone and fetch times, the effects can compound 
quickly without early detection. By leveraging these plugins, we can gain a clearer 
understanding of the repository’s state, allowing us to make informed decisions on repository maintenance and optimization. 
Join me as we uncover how these tools empower developers and DevOps teams to
keep their repositories lean, fast, and scalable.

[1]: https://gerrit.googlesource.com/plugins/git-repo-metrics/
[2]: https://github.com/GerritForge/ghs-metrics-collector

*[Fabio Ponciroli, Senior Engineer / GerritForge ](../speakers.md#fponciroli)*
