# Creating Quotas using Task Parker

I’ll briefly explain the life cycle of a task and how the new
Task Parker interface fits in. We’ll also look at a demo of a
quota implemented using Task Parker. Quotas are particularly
useful for admins who want to limit certain expensive tasks
to maintain the service’s health.

[1] https://gerrit-review.googlesource.com/Documentation/dev-plugins.html#taskParker

*[Kaushik Lingarkar, Engineer / Qualcomm Technologies, Inc](../speakers.md#kaushikl)*
