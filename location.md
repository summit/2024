# Gerrit User Summit 2024 - Location

## Hosted at Qualcomm

The Hackathon and User Summit are hosted at Qualcomm's campus in San Diego in
[Building AQ](https://maps.app.goo.gl/pcwWUwUjfMpBoMTZ8).

See below the full address of the location:

```
10145 Pacific Heights Blvd
San Diego, CA 92121
USA
```

## Getting there

Renting a car or using a ride service are the best options for getting around
San Diego.

## Check-in Instructions {#checkin}

Visitors will receive an email from Qualcomm Global Security to verify their
information ahead of the summit.

Visitors can use the Qualcomm parking garage outlined in the green rectangle on
the map below.

Building AQ (circled in blue on the map) does not have a reception desk, so
we'll be doing the visitor check-in at Building AX (marked with a purple star
on the map).

![Parking](images/bldgAQ_parking.png)

## Facility Resources

__Food and Drink__

Lunch will be provided on the roof of the nearby [Building AZ](https://maps.app.goo.gl/4c2GKVoPa6QzL5Li7)
during the User Summit. Breakfast and refreshments will be available at the
main user summit room.

__Internet__

Wifi will be available to all attendees. Visitors will receive Wifi
instructions when they check in.

## Hotels and Places to Stay

You can call and ask for the Qualcomm rate at the
[Country Inn & Suites San Diego North](https://www.choicehotels.com/california/san-diego/country-inn-suites-hotels/cak24)
```
5975 Lusk Blvd
San Diego, CA 92121
USA
```
