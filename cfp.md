# Propose a talk about Gerrit Code Review!

The User Summit is about people and their experiences in using Gerrit Code
Review for their teams and companies.

We are looking forward to receiving fantastic proposals from industry
experts about how you're using Gerrit, including upgrading to the latest
versions, integrating with your Development Pipeline(s), or any extensions
you've made to Gerrit with amazing plugins.

Other interesting experiences could include scaling Gerrit (via
multi-primary/multi-site, pull-replication/replication, Git/JGit, or some other
approach) and the entire Continuous Integration pipeline, including integration
with specific CI systems (Jenkins or others) and with issue tracking systems.

The talk can be submitted as a lightning talk (15 minutes) or a full length
talk (40 minutes). For a lightning talk, follow the steps below, but use the
`/lightning-talks` directory instead of `/sessions`.

## Can I present remotely?

Yes! Presentations will be streamed using Microsoft Teams (available via app or
browser) and you can deliver your presentation remotely by logging into the
Teams meeting using an account registered on [outlook.com](https://outlook.com).

## How to submit my proposal?

It is very simple, just clone this project, create a Markdown document under
the `/sessions` directory using the `template.md` as an example and then push
a change for review.

You need to be registered on [Gerrit Code Review](https://gerrit-review.googlesource.com)
in order to be able to be authorized to push changes for review.

Alternatively, you can create and edit a change in the Gerrit UI via the
`Create Change` button from the
[project page](https://gerrit-review.googlesource.com/admin/repos/summit/2024,commands).

**Example:**
```bash
$ git clone https://gerrit.googlesource.com/summit/2024 && (cd 2024 && f=`git rev-parse --git-dir`/hooks/commit-msg ; mkdir -p $(dirname $f) ; curl -Lo $f https://gerrit-review.googlesource.com/tools/hooks/commit-msg ; chmod +x $f)
$ cd 2024/sessions
$ cp template.md myamazingtalk.md
$ vi myamazingtalk.md

# make your edits ...

$ git add myamazingtalk.md & git commit -m 'This is my amazing talk'
$ git push origin HEAD:refs/for/master
```

## How my talk is going to be voted and selected?

The voting is open to everyone that is registered on
[Gerrit Code Review](https://gerrit-review.googlesource.com).
People will be able to post comments, ask questions and vote for it.

The top several rated proposals will become part of the Gerrit User Summit 2024
agenda.

## Will my talk be recorded and published?

We are intending to record the sessions and publish them to allow others to
watch online later on, even if they have not attended the User Summit. However,
if you do not wish to be recorded, just let us know by including a comment on
your talk proposal.

You are free to publish your slides anywhere on the web and we will just point
to them.
