# Gerrit User Summit 2024

The Gerrit Community is pleased to announce that Qualcomm Technologies, Inc.
(https://www.qualcomm.com) with the cooperation of GerritForge
(https://www.gerritforge.com), will be organizing this year's Gerrit User
Summit 2024 in San Diego, CA (USA)!

The Gerrit User Summit is the event that brings together Gerrit admins,
developers, practitioners, and the whole community, in one place, providing
attendees with the opportunity to learn, explore, network face-to-face, and
help shape the future of Gerrit development and solutions.

## User Summit

The User Summit will take place on October 10th and 11th and is open to anyone
who wishes to attend. We encourage all the members of the community or anyone
who is willing to learn and adopt Gerrit Code Review in their development
process to join us for this 2 day conference.

## Hackathon (by invitation only)

Preceding the User Summit is a 3-day Hackathon from October 7th to 9th reserved
for the current Gerrit contributors and maintainers plus anyone that is willing
to start contributing to the platform. The Hackathon goal is to bring together
the Gerrit contributors to enable discussion and collabortion on larger
features and architectural design challenges with faster communication in a
co-located environment.

> Please note that the seats for the Hackathon are limited and your request
> could be declined by the organizer. We will do our best, however, to get
> people in and help increase the community of contributors.

## Dates

| October 7th - 9th (Mon - Wed)        | October 10th & 11th (Thu & Fri) |
|--------------------------------------|---------------------------------|
| 3 day Hackathon (by invitation only) | 2 day User Summit (open to all) |

## Location

The Gerrit User Summit & Hackathon is hosted at the Qualcomm Technologies, Inc.
[campus in San Diego](https://maps.app.goo.gl/pcwWUwUjfMpBoMTZ8) in cooperation
with [GerritForge Inc.](http://www.gerritforge.com) and the
[Gerrit Community](https://groups.google.com/g/repo-discuss/c/MJynErl58WM).

### Remote attendance

If you're not able to join in person, you can participate remotely by joining
[this Microsoft Teams meeting](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjEyMmU1ZmEtN2EwNC00MDgyLWI2MzctNDk3ZTJkYzhmY2Iy%40thread.v2/0?context=%7b%22Tid%22%3a%2298e9ba89-e1a1-4e38-9007-8bdabc25de1d%22%2c%22Oid%22%3a%22603b9e0d-8aaf-4c6d-bef3-1fe27fd9885c%22%7d).