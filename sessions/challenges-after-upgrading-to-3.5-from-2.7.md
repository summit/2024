# Challenges after upgrade to 3.5 from 2.7

Last year, Qualcomm migrated its largest Gerrit instance from 2.7 to
3.5. In a previous talk, we discussed our optimizations to schema
upgrades, NoteDB migration, and offline reindexing. In this talk,
I’ll delve into the challenges we faced close to the upgrade date and
after going live. I’ll focus mainly on the issues we encountered with
our multi-primary setup, repositories on NFS, and JGit, as well as our
solutions. Also, I’ll explore the performance problems we faced and how
we overcame them.

I’ll also share some positives, such as what worked well after the
upgrade, including our use of Elasticsearch as the index backend
and some updates we made to the Elasticsearch module to meet our needs.

*[Kaushik Lingarkar, Engineer / Qualcomm Technologies, Inc](../speakers.md#kaushikl)*
