# Running k8s-gerrit in production

At the Gerrit User Summit 2023, we presented a prototype of k8s-gerrit along with our vision
for its future. Today, SAP has been successfully using it in production for six months.

This journey — from running stable releases on-premises to operating the tip of the master branch
in Kubernetes on GCP — has been a wild ride. Join me as I recount the challenges we encountered,
the benefits we've gained, and hear about the next steps we plan.

* *[Matthias Sohn, SAP](../speakers.md#msohn)*
