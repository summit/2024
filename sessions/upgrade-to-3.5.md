# Upgrade to Gerrit 3.5 takeaways

I will talk about issues found during 3.5 upgrade for a customer that resulted
in code changes in Gerrit 3.5.

*[Eryk Szymanski, Digital.ai](../speakers.md#eszymanski)*
