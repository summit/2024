# Migration from Gerrit 2.13 to 3.9 on a per-project basis: Challenges and Lessons Learned

The migration from Gerrit 2.13 to 3.9 presents a significant shift,
involving deep structural changes that can be challenging to navigate.
One of the key hurdles is the transition from the legacy ReviewDB to NoteDB,
which fundamentally changes how data is stored and managed.
In this talk, I will discuss our experience migrating across these versions
on a per-project basis to minimize downtime and ensure a smooth transition
for teams.

We’ll explore the technical challenges faced during the migration,
including data integrity issues, performance impacts, and the complexities
introduced by the major architectural shift. By sharing insights from this
process, attendees will gain a clearer understanding of how to plan,
execute, and mitigate risks when undertaking such a large-scale Gerrit
upgrade, especially when dealing with long-lived versions like Gerrit 2.13.

*[Fabio Ponciroli, Senior Engineer / GerritForge](../speakers.md#fponciroli)*