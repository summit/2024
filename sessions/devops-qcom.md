# Qualcomm's Gerrit management overview from Devops/SRE

We are going to talk about Qualcomm's Gerrit global footprint,
infra management, automated deployments, future deployment strategies,
monitoring, alerting, and problems.

* *[Vasdev Gullapalli, Qualcomm Inc.](../speakers.md#vasdevg)*
* *[Tushar Himmat Dahibhate, Qualcomm Inc.](../speakers.md#tdahibha)*
