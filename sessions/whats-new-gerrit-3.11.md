# What's new in Gerrit v3.11

The [Gerrit v3.11 release](https://gerrit-review.googlesource.com/c/homepage/+/438502/1/_posts/2024-10-03-gerrit-3.11-release-plan.md) cycle starts next week!

Nasser gives an overview of the improvements introduced,
including:

- Improved AI Suggested Fixes UI and plugin endpoints
- Gerrit Checks UI & API enhancements
- Multiple new and enhanced REST APIs
- New plugin extension points
- Performance improvements
- Notable bug fixes

*[Nasser Grainawi - Qualcomm Innovation Center, Inc.](../speakers.md#nasser)*
