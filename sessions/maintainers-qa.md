# Q&A with the Gerrit maintainers

Meet the Gerrit maintainers and ask those questions that you've always been
wanting to ask.

You can post your questions in the Q&A section of the
[MS Teams meeting](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjEyMmU1ZmEtN2EwNC00MDgyLWI2MzctNDk3ZTJkYzhmY2Iy%40thread.v2/0?context=%7b%22Tid%22%3a%2298e9ba89-e1a1-4e38-9007-8bdabc25de1d%22%2c%22Oid%22%3a%22603b9e0d-8aaf-4c6d-bef3-1fe27fd9885c%22%7d).

* *[Nasser Grainawi - Qualcomm Innovation Center, Inc.](../speakers.md#nasser)*
* *[Kaushik Lingarkar - Qualcomm Technologies, Inc.](../speakers.md#kaushikl)*
* *[Luca Milanesio - GerritForge, Inc.](../speakers.md#luca)*

