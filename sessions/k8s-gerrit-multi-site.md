# Adding multi-site K8s-Gerrit support

K8s-Gerrit is an incredibly useful way to simplifying Gerrit management
operations. Up to know it has only supported high-availability set-ups, but
we've worked hard to support multi-site installations and are excited to show
you a live demo of of three kubernetes pods serving incoming traffic without any
shared filesystem..

*[Daniele Sassoli, Senior Engineering Manager / GerritForge
Inc.](../speakers.md#dsassoli)*
